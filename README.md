# A Progressive Web Application Example

Practice code for Udemy course [Progressive Web Apps - The Concise PWA Masterclass](https://www.udemy.com/progressive-web-apps/)
by [Ray Viljoen](https://twitter.com/AcademyStack).
