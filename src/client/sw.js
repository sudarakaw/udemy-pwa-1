// Service Worker
//
// IMPORTANT: Service Worker's scope (on static files & resources) are limited
// to the directory structure it's located. Therefor it's good practice to have
// the Service Worker file in the root directory of the web application.

const
  SW_VERSION = 8,
  CACHE_ID = `pwa-cache-${SW_VERSION}`,

  // Static assets to cache on install
  staticCache = [
    '/',
    '/index.html',
    '/page2.html',
    '/assets/css/style.css',
    '/assets/img/thumb.png',
    '/assets/img/placeholder.png',
    '/assets/js/main.js',
  ]

// CACHING STRATEGIES
self.addEventListener('fetch', e => {

  // Strategy 1 - Cache only static assets / App Shell
  // WARNING: If user or some process removes and item from cache, this method
  // can't fetch it again.
  // e.respondWith(
  //   caches.match(e.request)
  // )

  // Strategy 2 - Cache with Network as a fallback.
  e.respondWith(
    caches.match(e.request)
      .then(res => {
        if(res) {
          return res
        }

        return fetch(e.request)
          .then(newRes => {
            caches.open(CACHE_ID)
              .then(cache => cache.put(e.request, newRes))

            // NOTE: we clone the `newRes` here because this line get executed
            // before the above promise resolve.
            return newRes.clone()
          })
      })
  )

  // Strategy 3 - Network with Cache as a fallback.
  // WARNING: This method will work POORLY in a bad/slow network conditions as
  // the fetch request will stall before timeout occur & server from cache.
  // e.respondWith(
  //   fetch(e.request)
  //     .then(res => {
  //       caches.open(CACHE_ID)
  //         .then(cache => cache.put(e.request, res))
  //
  //       // NOTE: we clone the `res` here because this line get executed
  //       // before the above promise resolve.
  //       return res.clone()
  //     })
  //     .catch(_ => caches.match(e.request))
  // )

  // Strategy 4 - Cache with Network update
  // NOTE: this method is wastefull as we always make fetch requests to the
  // server to see if t here are updated resources.
  // WARNING: Resources we see/execute are always 1 reload behind. i.e to apply
  // and see the changes we have to reload twice.
  // e.respondWith(
  //   caches.open(CACHE_ID)
  //     .then(cache => {
  //       // Return response from cache
  //       return cache.match(e.request)
  //         .then(res => {
  //           // Ask server for updated resource
  //           const
  //             updatedRes = fetch(e.request)
  //               .then(newRes => {
  //                 // NOTE: cache is already open and following line runs first.
  //                 // So we need to clone the resource here.
  //                 cache.put(e.request, newRes.clone())
  //
  //                 return newRes
  //               })
  //
  //           return res || updatedRes
  //         })
  //     })
  // )

  // Strategy 5 - Cache and Network race with offline content
  // NOTE: in a device with slow storage, network fetch request may resolve
  // before the cache match.
  // const
  //   firstResponse = new Promise((resolve, reject) => {
  //     let
  //       firstRejectionReceived = false
  //
  //     const
  //       rejectOnce = () => {
  //         if(firstRejectionReceived) {
  //           if(e.request.url.match('/assets/img/thumb.png')) {
  //             resolve(caches.match('/assets/img/placeholder.png'))
  //           }
  //           else {
  //             reject('No response received.')
  //           }
  //         }
  //         else {
  //           firstRejectionReceived = true
  //         }
  //       }
  //
  //     // Try network
  //     fetch(e.request)
  //       .then(res => {
  //         res.ok() ? resolve(res) : rejectOnce()
  //       })
  //       .catch(rejectOnce)
  //
  //     // Try cache
  //     caches.match(e.request)
  //       .then(res => {
  //         res ? resolve(res) : rejectOnce()
  //       })
  //       .catch(rejectOnce)
  //
  //   })
  //
  // e.respondWith(firstResponse)

})

// SW install and cache static assets
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(CACHE_ID)
      .then(cache => cache.addAll(staticCache))
  )
})

// SW activate and cache cleanup
self.addEventListener('activate', e => {
  const
    cacheCleaned = caches.keys()
      .then(keys => {
        keys.forEach(key => {
          if(key !== CACHE_ID) {
            return caches.delete(key)
          }
        })
      })

  e.waitUntil(cacheCleaned)
})

// self.addEventListener('message', e => {
//   // if('UPDATE_SELF' === e.data) {
//   //   console.log('SW: new version updating')
//   //
//   //   self.skipWaiting()
//   // }
//
//   self.clients.matchAll()
//     .then(clients => {
//       clients.forEach(client => {
//         if(e.source.id === client.id) {
//           client.postMessage('SW: hello there')
//         }
//         else {
//           client.postMessage(`SW: we have a new guy "${e.source.id}"`)
//         }
//       })
//     })
// })
//
//
// self.addEventListener('push', e => {
//   // console.log('SW: push received', e)
//
//   const
//     n = self.registration.showNotification('SW: notification from push event')
//
//   e.waitUntil(n)
// })
