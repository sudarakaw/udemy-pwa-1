// Web push module

const
  webpush = require('web-push'),
  vapid = require('./vapid'),

  subscription = {
    'endpoint': 'https://fcm.googleapis.com/fcm/send/fhS9vRwaV-c:APA91bHaXi0eUkyi_IFmRiTMU29TgKyBOJOqbyhKO-OuqYmxC6qo9Qp5EWF0wjVYTvlka-bvjTzX5ao5WMDlbcvQoIS2A03554PzGl7zGfu50ncwZPD0r-VKHBr20AJqxG0lp-lahjmq',
    'keys': {
      'p256dh': 'BJjhETiyQPyEC0Qb-gNUdFZz8AKXEi1Y_h_iIbkyK0ojRTCFqrzQrouS3GgwUvQdp5Is7Ta9_oGnt8BTl39fmxs',
      'auth': 'Ilhc2nyzyVhp9Xnc0c-Xbg'
    }
  }

// Configure keys
webpush.setVapidDetails(
  'mailto:example@example.com',
  vapid.publicKey,
  vapid.privateKey
)

webpush.sendNotification(subscription, 'Notification from push server')
